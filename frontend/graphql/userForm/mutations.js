import gql from 'graphql-tag'

const getExemption = gql`
  mutation($userForm: UserFormInputType, $user: UserInputType) {
    handleUserForm(userForm: $userForm, user: $user) {
      exemptions {
        pension {
          id
          name
          description
          type
        }
        livingConditions {
          id
          name
          description
          type
        }
        familyState {
          id
          name
          description
          type
        }
        children {
          id
        }
        disability {
          id
          name
          description
          type
        }
        work {
          id
          name
          description
          type
        }
      }
    }
  }
`
export { getExemption }
