from hashlib import sha256

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, _user_has_perm, UserManager
from django.db import models
from django.utils.crypto import get_random_string, constant_time_compare


class User(AbstractBaseUser, PermissionsMixin):
    login = models.CharField('Логин', max_length=64, unique=True)
    first_name = models.CharField('Имя', max_length=50)
    last_name = models.CharField('Фамилия', max_length=50)
    patronym = models.CharField('Отчество', max_length=50, null=True, blank=True)
    email = models.CharField('Электронная почта', max_length=64, null=True, default=None, blank=True)
    password = models.CharField(max_length=64)
    salt = models.CharField(max_length=64)
    is_active = models.BooleanField(default=False)
    date_joined = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'login'
    EMAIL_FIELD = 'email'

    objects = UserManager()

    def set_password(self, raw_password):
        salt = get_random_string(64)
        hasher = sha256()
        raw_password = raw_password + '_' + salt
        hasher.update(raw_password.encode('utf-8'))
        self.salt = salt
        self.password = hasher.hexdigest()
        return self

    def check_password(self, raw_password):
        hasher = sha256()
        raw_password = raw_password + '_' + self.salt
        hasher.update(raw_password.encode('utf-8'))
        result = constant_time_compare(hasher.hexdigest(), self.password)
        return result

    def has_perm(self, perm, obj=None):
        # Не уволенные суперпользователи имеют весь доступ
        if not self.is_active and self.is_superuser:
            return True
        return _user_has_perm(self, perm, obj)

    @property
    def is_staff(self):
        return self.is_superuser




class Pensioner(models.Model):
    state = models.BooleanField(default=False)
    child_of_war = models.BooleanField(default=False)
    tags = models.TextField(default='', blank=True)

    def save(self, *args, **kwargs):
        self.tags = ''
        if self.state:
            self.tags += ' state'
        if self.child_of_war:
            self.tags += ' chid_of_war'
        super(Pensioner, self).save(*args, **kwargs)



class Work(models.Model):
    expirience = models.SmallIntegerField(null=True, blank=True)
    veteran_of_labour = models.BooleanField(default=False)
    is_working = models.BooleanField(default=False)
    tags = models.TextField(default='', blank=True)

    def save(self, *args, **kwargs):
        self.tags = ''
        if self.veteran_of_labour:
            self.tags += ' veteran_of_labour'
        if self.is_working:
            self.tags += ' working'
        if  self.expirience and self.expirience > 40:
            self.tags += ' expirienced'
        super(Work, self).save(*args, **kwargs)


class Disability(models.Model):
    FIRST = 1
    SECOND = 2
    THIRD = 3
    FOURTH = 4

    DISABILITY_GROUPS = (
        (FIRST, 'Первая'),
        (SECOND, 'Вторая'),
        (THIRD, 'Третья'),
        (FOURTH, 'Четвертая'),
    )

    group = models.SmallIntegerField(choices=DISABILITY_GROUPS, blank=True, null=True)
    tags = models.TextField(default='', blank=True)

    def save(self, *args, **kwargs):
        self.tags = ''
        if self.group:
            self.tags += str(self.group)
        super(Disability, self).save(*args, **kwargs)


class FamilyState(models.Model):
    NOT_MARRIED = 'NM'
    MARRIED = 'M'
    DIVORCED = 'D'
    WIDOWED = 'W'

    STATE_CHOICES  = (
        (NOT_MARRIED, 'Не женат'),
        (MARRIED, 'Женат'),
        (DIVORCED, 'В разводе'),
        (WIDOWED, 'Вдова'),
    )
    state = models.CharField(choices=STATE_CHOICES, max_length=10)
    tags = models.TextField(default='', blank=True)

    def save(self, *args, **kwargs):
        self.tags = ''
        if self.state:
            self.tags += self.state
        super(FamilyState, self).save(*args, **kwargs)

class LivingConditions(models.Model):
    has_house = models.BooleanField(default=True)
    is_needy = models.BooleanField(default=False)
    tags = models.TextField(default='', blank=True)

    def save(self, *args, **kwargs):
        self.tags = ''
        if self.has_house:
            self.tags += 'has_house'
        if self.is_needy:
            self.tags.append('needy')


class UserProfile(models.Model):

    MALE = 'M'
    FEMALE = 'F'
    SEX_CHOICES = (
        (MALE, 'Мужчина'),
        (FEMALE, 'Женщина'),
    )

    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    sex = models.CharField(choices=SEX_CHOICES, null=True, blank=True, max_length=10)
    born_date = models.DateField(null=True, blank=True)
    pensioner = models.OneToOneField(Pensioner, null=True, blank=True, on_delete=models.CASCADE)
    work = models.OneToOneField(Work, null=True, blank=True, on_delete=models.CASCADE)
    disability = models.OneToOneField(Disability, null=True, blank=True, on_delete=models.CASCADE)
    family_state = models.OneToOneField(FamilyState, null=True, blank=True, on_delete=models.CASCADE)
    donor = models.BooleanField(default=False)
    living_conditions = models.OneToOneField(LivingConditions, null=True, blank=True, on_delete=models.CASCADE)


class Children(models.Model):
    user_profile = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=True, blank=True)
    age = models.SmallIntegerField(blank=True, null=True)


class Exemption(models.Model):
    PENSION = 'P'
    LIVING_CONDITIONS = 'LC'
    FAMILY_STATE = 'FS'
    CHILDREN = 'C'
    DISABILITY = 'D'
    WORK = 'W'
    TYPES_CHOICES = (
        (PENSION, 'Пенсия'),
        (LIVING_CONDITIONS, 'Жилищные условия'),
        (FAMILY_STATE, 'Семейное положение'),
        (CHILDREN, 'Дети'),
        (DISABILITY, 'Инвалидность'),
        (WORK, 'Работа')

    )
    name = models.CharField(max_length=100)
    description = models.TextField()
    type = models.CharField(choices=TYPES_CHOICES, max_length=10, null=True, blank=True)
    tags = models.TextField(default='', blank=True)

class ExtraLink(models.Model):
    name = models.CharField(max_length=50)
    url = models.CharField(max_length=500)
    exemption = models.ForeignKey(Exemption, on_delete=models.CASCADE)

