# Generated by Django 2.2.3 on 2019-07-20 16:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_exemption_extralink'),
    ]

    operations = [
        migrations.AddField(
            model_name='exemption',
            name='tags',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AddField(
            model_name='pensioner',
            name='tags',
            field=models.TextField(blank=True, default=''),
        ),
    ]
