from django.contrib import admin
from .models import User, Exemption


admin.site.register(User)
admin.site.register(Exemption)
