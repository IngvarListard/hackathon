import graphene
from django.contrib.auth import authenticate, login, logout

from backend.users.models import User, UserProfile, Pensioner, Work, Disability, FamilyState, LivingConditions, \
    Exemption
from backend.users.schema.types import BasicUserType, UserFormInputType, ExemptionsByTypeType, UserInputType


class CreateUser(graphene.Mutation):
    test = graphene.String()

    class Arguments:
        a = graphene.String()

    def mutate(self, info, a):
        return a


class Login(graphene.Mutation):
    user = graphene.Field(BasicUserType)
    success = graphene.Boolean()

    class Meta:
        description = 'Вход пользователя в систему'

    class Arguments:
        login = graphene.String(required=True)
        password = graphene.String(required=True)

    def mutate(self, info, **kwargs):
        print(kwargs)
        user = authenticate(info.context, **kwargs)
        if user is not None:
            login(info.context, user)
        return Login(user=user, success=user is not None)


class Logout(graphene.Mutation):
    class Meta:
        description = 'Выход пользователя из системы'

    success = graphene.Boolean(required=True, description='Успех операции')

    @staticmethod
    def mutate(root, info):
        if info.context.user.is_authenticated:
            logout(info.context)
            return Logout(success=True)
        else:
            return Logout(success=False)


class HandleUserForm(graphene.Mutation):
    exemptions = graphene.Field(ExemptionsByTypeType)

    class Arguments:
        user_form = UserFormInputType()
        user = UserInputType()

    def mutate(self, info, **kwargs):
        if kwargs.get('user'):
            print(1234)
            user = kwargs.pop('user')
            password = user.pop('password')
            user = User(**user)
            user = user.set_password(password)
            user.is_active = True
            user.save()

            user_profile = UserProfile(user=user, **kwargs.pop('user_profile', {}))
            user_profile.save()

            try:
                user_profile.pensioner = Pensioner(**kwargs.pop('pensioner', {}))
                user_profile.pensioner.save()
            except:
                pass

            try:
                user_profile.work = Work(**kwargs.pop('work', {}))
                user_profile.work.save()
            except:
                pass

            try:
                user_profile.family_state = FamilyState(**kwargs.pop('family_state', {}))
                user_profile.family_state.save()
            except:
                pass

            try:
                user_profile.disability = Disability(**kwargs.pop('disability', {}))
                user_profile.disability.save()
            except:
                pass

            try:
                user_profile.living_conditions = LivingConditions(**kwargs.pop('living_conditions', {}))
            except:
                pass

        return HandleUserForm(exemptions=ExemptionsByTypeType(
            pension=Exemption.objects.filter(type=Exemption.PENSION),
            living_conditions=Exemption.objects.filter(type=Exemption.LIVING_CONDITIONS),
            family_state=Exemption.objects.filter(type=Exemption.FAMILY_STATE),
            disability=Exemption.objects.filter(type=Exemption.DISABILITY),
            work=Exemption.objects.filter(type=Exemption.WORK)
        ))


class Mutation(graphene.ObjectType):
    create_user = CreateUser.Field()
    login = Login.Field()
    logout = Logout.Field()
    handle_user_form = HandleUserForm.Field()
