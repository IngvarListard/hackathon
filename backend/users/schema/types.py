import graphene
import graphene_django
from ..models import *

from backend.users.models import User


class UserType(graphene.Interface):

    class Meta:
        description = 'Абстрактный интерфейс пользователя'

    id = graphene.Int(required=True)
    first_name = graphene.String()
    last_name = graphene.String()
    login = graphene.String()
    email = graphene.String()
    is_active = graphene.String()
    is_superuser = graphene.String()


class BasicUserType(graphene_django.DjangoObjectType):

    class Meta:
        model = User
        interface = (UserType,)
        description = 'Объект пользователя с базовой информацией'
        only_fields = ('id', 'first_name', 'last_name', 'is_active', 'is_superuser', 'email')


class PensionerType(graphene_django.DjangoObjectType):

    class Meta:
        model = Pensioner


class WorkType(graphene_django.DjangoObjectType):
    class Meta:
        model = Work


class DisabilityType(graphene_django.DjangoObjectType):
    class Meta:
        model = Disability


class FamilyStateType(graphene_django.DjangoObjectType):
    class Meta:
        model = FamilyState


class LivingConditionsType(graphene_django.DjangoObjectType):
    class Meta:
        model = LivingConditions


class UserProfileType(graphene_django.DjangoObjectType):
    class Meta:
        model = UserProfile


class ChildrenType(graphene_django.DjangoObjectType):
    class Meta:
        model = Children


class ExemptionsType(graphene_django.DjangoObjectType):
    class Meta:
        model = Exemption


class PensionerInputType(graphene.InputObjectType):
    state = graphene.Boolean()
    child_of_war = graphene.Boolean()

class WorkInputType(graphene.InputObjectType):
    expirience = graphene.Float()
    veteran_of_labour = graphene.Boolean()
    is_working = graphene.Boolean()


class DisabilityInputType(graphene.InputObjectType):
    group = graphene.Float()


class FamilyStateInputType(graphene.InputObjectType):
    state = graphene.String()


class LivingConditionsInputType(graphene.InputObjectType):
    has_house = graphene.Boolean()
    is_needy = graphene.Boolean()


class UserProfileInputType(graphene.InputObjectType):
    sex = graphene.String()
    born_date = graphene.Date()
    donor = graphene.Boolean()


class ChildrenInputType(graphene.InputObjectType):
    age = graphene.Float()


class UserFormInputType(graphene.InputObjectType):
    user_profile = graphene.Field(UserProfileInputType)
    pensioner = graphene.Field(PensionerInputType)
    work = graphene.Field(WorkInputType)
    disability = graphene.Field(DisabilityInputType)
    family_state = graphene.Field(FamilyStateInputType)
    living_condition = graphene.Field(LivingConditionsInputType)
    children = graphene.List(ChildrenInputType)


class ExemptionsByTypeType(graphene.ObjectType):
    pension = graphene.List(ExemptionsType)
    living_conditions = graphene.List(ExemptionsType)
    family_state = graphene.List(ExemptionsType)
    children = graphene.List(ExemptionsType)
    disability = graphene.List(ExemptionsType)
    work = graphene.List(ExemptionsType)


class UserInputType(graphene.InputObjectType):
    first_name = graphene.String()
    last_name = graphene.String()
    patronym = graphene.String()
    login = graphene.String()
    password = graphene.String()
