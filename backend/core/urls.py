"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView
from django.conf.urls.static import static
import mimetypes
from os.path import join
from django.http import HttpResponse
from django.conf.urls import url

admin.site.site_header = 'Администрирование СУП «Синергия»'


def _csrf_exempt(view_func):
    """
    Функция оборачивает GraphQL API в вызов csrf_exempt,
    который позволяет избавиться от ошибок при запросах с другого хоста (в режиме разработки).
    На продакшене функция возвращает необернутый view_func, чтобы обеспечить защиту от CSRF.
    CSRF - https://en.wikipedia.org/wiki/Cross-site_request_forgery
    """
    if settings.DEBUG:
        return csrf_exempt(view_func)
    else:
        return view_func


def download(request):
    path_file = r'C:\Users\Mikhail-PC\IdeaProjects\hackathon1\backend\browser.crx'
    my_file = open(path_file, 'rb')
    file_name = my_file.name.split('\\')[-1]
    content_type = mimetypes.guess_type(file_name)[0]
    response = HttpResponse(my_file.read(), content_type='application/x-chrome-extension')
    response['Content-Disposition'] = ('attachment; filename="%s"' % file_name).encode('utf-8')
    return response


urlpatterns = [
    path('api_v1', _csrf_exempt(GraphQLView.as_view(
        graphiql=not settings.GRAPHQL_BATCH,
        batch=settings.GRAPHQL_BATCH)
    )),
    path('admin/', admin.site.urls),
    url(r'^download_file/$', download),
] + (static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
     + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT))
